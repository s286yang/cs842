import warnings
import math
import torch
from pyro.infer.trace_elbo import Trace_ELBO
from pyro.infer.util import check_fully_reparametrized, is_validation_enabled

class TraceRenyi_ELBO(Trace_ELBO):
    
    def __init__(self,
                 alpha=0,
                 num_particles=2,
                 max_plate_nesting=float('inf'),
                 vectorize_particles=True,
                 strict_enumeration_warning=True):

        if alpha == 1:
            raise ValueError("The order alpha should not be equal to 1. Please use Trace_ELBO class"
                             "for the case alpha = 1.")
        self.alpha = alpha
        super().__init__(num_particles=num_particles,
                         max_plate_nesting=max_plate_nesting,
                         vectorize_particles=vectorize_particles,
                         strict_enumeration_warning=strict_enumeration_warning)
    
    def loss(self, model, guide, *args, **kwargs):
        raise NotImplementedError("Loss method for TraceRenyi_ELBO not implemented")


    def _differentiable_loss_particle(self, model_trace, guide_trace):
        if not self.vectorize_particles:
            raise NotImplementedError("TraceRenyi_ELBO only implemented for vectorize_particles==True")

        log_p, log_q = 0, 0

        for name, site in model_trace.nodes.items():
            if site["type"] == "sample":
                site_log_p = site["log_prob"].reshape(self.num_particles, -1).sum(-1)
                log_p = log_p + site_log_p

        for name, site in guide_trace.nodes.items():
            if site["type"] == "sample":
                site_log_q = site["log_prob"].reshape(self.num_particles, -1).sum(-1)
                log_q = log_q + site_log_q
                if is_validation_enabled():
                    check_fully_reparametrized(site)

        log_weights = (1. - self.alpha) * (log_p - log_q)
        log_mean_weight = torch.logsumexp(log_weights, dim=0) - math.log(self.num_particles)
        surrogate_loss = -log_mean_weight.sum() / (1. - self.alpha)

        return float('inf'), surrogate_loss